#!/usr/bin/env python3

import csv
from os.path import join, realpath
from random import choice
from prettytable import PrettyTable

filename = "bar_rennes_26_11_2021.csv"
filepath = join(realpath("data"), filename)

fields = []
bars = []

with open(filepath) as f:
    data = csv.reader(f, delimiter=";", quotechar='"')
    
    fields = next(data)

    for row in data:
        bars.append(row)

table = PrettyTable()
table.field_names = fields
table.add_row(choice(bars))
table.align = "l"

print(table)
